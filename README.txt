Amara's code test
=================

The aim of this code test is to get an idea about how you like to code, it's a
way for you to show off your 1337 skillz. It's not using any framework (unless
Zero counts) so the whole thing is hopefully understandable quickly.

At the moment, the code displays some text which describes the current reviews
for an Amara product when run from the command line, for example:

php bootstrap.php "\Amara\Reviews\Action?id=42687"

This should output something like:

"There are 21 reviews and the average is 100%"

Product id 42687 is for a "Vera Kettle", you can see it on the Amara site
here:

https://www.amara.com/products/vera-kettle

Just in case you'd like some other ids:

42690 is a "Vela Food Blender" (to match the kettle!)
https://www.amara.com/products/vela-food-blender-red

Those reviews are coming from Feefo who collect customer reviews for Amara.


The objectives
==============

At the moment, every time we run the command it fetches the reviews from Feefo
and displays some text about them.  The text output should stay the same (well,
unless the reviews change!)

Now we'd like to store the results of the Feefo request and also make them
available to other parts of the system so we can, for example, display them on
the product listing pages without making (say) 45 requests to Feefo on each
page view.  They should be refreshed after a certain time though, let's say 24
hours.

Additionally, at the moment the \Amara\Reviews\Action class contains the code
which fetches the review data from Feefo, this should be moved elsewhere with
some sort of nice API.  That API should also allow the retrieval of reviews for 
multiple product ids and provide some way of invalidating the cached product 
reviews.

Finally, please list any issues you can see with the code which actually 
retrieves the reviews.

This will need some sort of database/cache as well, so please include anything
required to set them up.

