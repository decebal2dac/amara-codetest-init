<?php

namespace Amara\Reviews;

use Zero\Action as ZeroAction;
use \FeefoApi\Controller\FeefoController;

/**
 * Action
 *
 * This displays a short description of a product's reviews when an id is passed
 * in.
 */
class Action implements ZeroAction
{
	/**
	 * @param array $request
	 * @return string
	 */
	public function execute($request)
    {
        $params = array('id' => $request['id']);
        $apiController = new FeefoController();

		return $apiController->processXml($params);
	}
}
