<?php

require_once('vendor/autoload.php');
require_once('Resources/start/global.php');

$controller = new \Zero\Controller();

echo $controller->dispatch($argv);
